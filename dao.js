var cradle = require('cradle');

if (process.env.CLOUDANT_URL) {
    var db = new (cradle.Connection)(process.env.CLOUDANT_URL, 80).database('friends');
} else {
    db = new (cradle.Connection)().database('friends');
}

db.exists(function(err, exists) {
    if (err) {
        console.log('There was an error checking if the database exists.', err);
    } else if (!exists) {
        db.create();
        console.log('The database did not exist yet; it has been created.');
    }

    db.save('_design/friends', {
    	all: {
    		map: function(doc) {
    			if (doc.type === 'friend' && doc.name) emit(doc.name, doc);
    		}
    	},
    	byGitHubName: {
    		map: function(doc) {
    			if (doc.type === 'friend' && doc.gitHubName) emit(doc.gitHubName, doc);
    		}
    	}
    });

    db.save('_design/pages', {
        all: {
            map: function(doc) {
                if (doc.type === 'page' && doc._id) emit(doc._id, doc);
            }
        }
    });

});

exports.db = db;
