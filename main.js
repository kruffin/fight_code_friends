var restify = require('restify');
var dao = require('./dao.js');
var fs = require('fs');
var jsdom = require('jsdom');
var globalFriends = [{name: "Kevin Ruffin", gitHubName: 'kruffin', url: 'http://fightcodegame.com/profile/kruffin'},
				     {name: "David Liu", gitHubName: 'dliu120', url: 'http://fightcodegame.com/profile/dliu120'}];

var templateIndex = fs.readFileSync(__dirname + '/index.html', 'utf-8');
var jqueryJs = fs.readFileSync(__dirname + '/lib/jquery.js');
var weldJs = fs.readFileSync(__dirname + '/lib/weld.js');
var css = fs.readFileSync(__dirname + '/style/style.css');

var pageCache = undefined;

var server = restify.createServer({
    name: 'Fight Code Friends',
    version: '1.0.0',
    formatters: {
        'application/json': function(req, res, body){
        	if (body) {
	            if(req.params.callback){
	                var callbackFunctionName = req.params.callback.replace(/[^A-Za-z0-9_\.]/g, '');
	                return callbackFunctionName + "(" + JSON.stringify(body) + ");";
	            } else {
	                return JSON.stringify(body);
	            }
        	} else {
            	return 'Not found.';
            }
        },
        'text/html': function(req, res, body){
        	if (body) {
            	return body;
            } else {
            	return 'Not found.';
            }
        },
        'application/x-javascript': function(req, res, body){
        	if (body) {
            	return body;
            } else {
            	return 'Not found.';
            }
        },
        'text/css': function(req, res, body){
        	if (body) {
            	return body;
            } else {
            	return 'Not found.';
            }
        },
        'image/png': function(req, res, body){
        	if (body) {
            	return body;
            } else {
            	return 'Not found.';
            }
        }
    }
});

function writeOut(res, code, type, content) {
	res.contentType = type;
	res.header('Content-Type',type);
	res.send(code, content);
}

server.use(restify.bodyParser());

function getFriendData(callback) {
	if (process.env.LOCAL) {
		callback(globalFriends);
	} else {
		dao.db.view('friends/all', function(err, result) {
			var friends = [];
			if (err) {
				res.send(500, 'Boing, fail snail!');
				callback(null);
			} else {
				result.forEach(function(row) {
					friends.push({
						name: row.name,
						gitHubName: row.gitHubName,
						url: 'http://fightcodegame.com/profile/' + row.gitHubName
					});
				});
				callback(friends);
			}
		});
	}
}

function getFriends(req, res, next) {
	getFriendData(function(friends) { 
		if (friends) {
			res.json(friends);
		}
		return next();
	});
};

function getByGitHubName(name, callback) {
	if (process.env.LOCAL) {
		callback(globalFriends);
	} else {
		dao.db.view('friends/byGitHubName', { key: name }, function(err, result) {
			var friends = [];
			if (err) {
				res.send(500, 'Boing, fail snail!');
				callback(null);
			} else {
				result.forEach(function(row) {
					friends.push(row);
				});
				console.log("Found friends|github: " + name + "|" + JSON.stringify(friends));
				callback(friends);
			}
		});
	}
}

function addFriend(req, res, next) {
	if (!req.params.name || !req.params.gitHubName) {
		res.send(400, '400 - Expected two parameters: { name, gitHubName }.');
		return next();
	}
	var f = { type: 'friend',
			  name: req.params.name,
			  gitHubName: req.params.gitHubName
			};
	if (process.env.LOCAL) {
		globalFriends.push(f);
		setTimeout(function() {
			loadPageFromDB(function() { console.log('Page generated successfully.'); }, 
			   			   function() { console.log('Page was not generated successfully.'); });
		}, 1000);
		res.json(f);
		return next();
	} else {
		// Check if friend exists
		getByGitHubName(f.gitHubName, function(friends) {
			if (friends && friends.length > 0) {
				friends[0].name = f.name;
				friends[0].gitHubName = f.gitHubName;
				var id = friends[0]._id;
				var rev = friends[0]._rev;
				friends[0]._id = undefined;
				friends[0]._rev = undefined;

				dao.db.save(id, rev, friends[0], function(err, result) {
					  	if (err) {
					  		res.send(500, 'Boing, fail snail!');
					  	} else {
					  		res.json(f);
					  		setTimeout(function() {
								loadPageFromDB(function() { console.log('Page generated successfully.'); }, 
								   			   function() { console.log('Page was not generated successfully.'); });
							}, 1000);
					  	}
					  	return next();
				});
			} else {
				dao.db.save(f, 
						function(err, result) {
						  	if (err) {
						  		res.send(500, 'Boing, fail snail!');
						  	} else {
						  		res.json(f);
						  		setTimeout(function() {
									loadPageFromDB(function() { console.log('Page generated successfully.'); }, 
								   				   function() { console.log('Page was not generated successfully.'); });
								}, 1000);
						  	}
						  	return next();
						});
			}
		});
	}
};

function scrape(callback) {
	getFriendData(function(friends) {
		var timerOffset = 0;
		var tanks = [];
		friends.forEach(function(f) {
			setTimeout((function(timerOffset) { return function() {
				jsdom.env(f.url, ['http://code.jquery.com/jquery-1.9.1.min.js'],  
					function(err, window) {
						if (err) {
							console.log('Error hitting url: ' + f.url + '|' + err);
						} else {
							var $ = window.jQuery;
							$('.robots').each(function() {
								$(this).find('li').each(function() {
									var tank = $(this);
									var rank = tank.find('.ranking')[0].innerHTML;
									var algorithm = tank.find('.name')[0].innerHTML;
									var score = tank.find('.score .text')[0].innerHTML.replace(/\n/g, '');
									var t = { ranking: rank,
												 score: score,
												 algorithm: algorithm,
												 player: f.name
												 };
									console.log(JSON.stringify(t));
									tanks.push(t);
								});
							});
						}
						console.log('offset: ' + timerOffset + '|friends length: ' + friends.length);
						if (timerOffset === friends.length - 1) {
							// The last friend to scrape
							// Sort the tanks by rank
							tanks = tanks.sort(function(a, b) {
								var arank = a.ranking.replace(/# /g, '');
								var brank = b.ranking.replace(/# /g, '');
								try {
									arank = parseInt(arank, 10);
								} catch (err) {
									// not a number
									arank = undefined;
								}
								try {
									brank = parseInt(brank, 10);
								} catch (err) {
									// not a number
									brank = undefined;
								}
								if (arank === undefined) {
									if (brank === undefined) {
										return 0;
									} else {
										return 1;
									}
								} else if (brank === undefined) {
									return -1;
								} else {
									return arank - brank;
								}
							});
							callback(friends, tanks);
						}
				});
			}})(timerOffset), 10000 * timerOffset);
			timerOffset += 1;
		});
	});
};

function generatePage(callback, errorCallback) {
	scrape(function(friends, tanks) {
		console.log('Scraping done; generating page now.');
		jsdom.env(templateIndex, ['./lib/jquery.js', './lib/weld.js'],
		function(err, window) {
			if (err) {
				return errorCallback();
			} else {
				var $ = window.jQuery;
				window.weld($('.friend')[0], friends, { alias: { url: function(parent, element, key, value) {
					if (key === 'url') {
						element.getElementsByClassName('url')[0].setAttribute('href', value);
						return true;
					}
				}}});
				var now = new Date();
				window.weld($('.generation')[0], 
					{timestamp: '' + ('0' + now.getHours()).slice(-2) + ":" + 
									 ('0' + now.getMinutes()).slice(-2) + " | " + 
									 (now.getMonth() + 1) + "/" + 
									 now.getDate() + "/" + 
									 now.getFullYear() });

				window.weld($('.tank')[0], tanks);

				if (window.document.innerHTML === undefined) {
					window.close();
					return errorCallback();
				} else {
					if (process.env.LOCAL) {
						pageCache = { type: 'page', 
									  content: window.document.innerHTML };
						window.close();
						console.log('Page generated; performing callback.');
						return callback();
					} else {
						dao.db.save('index', { type: 'page', 
											   content: window.document.innerHTML },
											   function(err, result) {
							pageCache = result.value;
							window.close();
							console.log('Page generated; performing callback.');
							return callback();
						});
					}
				}
			}
		});
	});
};

function onScrape(req, res, next) {
	setTimeout(function() {
		generatePage(function() {
			console.log('Forced import finished successfully.');
		}, function() {
			console.log('Forced import failed.');
		});
	}, 1000);

	res.send(200, 'Importing started...');
	return next();
}

function getFriendPage(req, res, next) {
	if (pageCache === undefined) {
		console.log('Cache not generated yet.');
		loadPageFromDB(function() {
			console.log('Cache has been generated.');
			writeOut(res, 200, 'text/html', pageCache.content);
			return next();
		}, function() {
			res.send(500, 'Boing, fail snail');
			return next(err);
		});
	} else {
		writeOut(res, 200, 'text/html', pageCache.content);
		return next();
	}
};

function loadPageFromDB(callback, errorCallback) {
	if (process.env.LOCAL) {
		return generatePage(callback, errorCallback);
	} else {
		dao.db.view('pages/all', { key: 'index' }, function(err, result) {
			if (err) {
				console.log('Error attempting to get the index page from the DB.');
				console.log(err);
				return errorCallback();
			} else if (result === undefined || result.length <= 0) {
				// It is not in the database
				return generatePage(callback, errorCallback);
			} else {
				console.log('Caching page as: ' + JSON.stringify(result[0].value));
				pageCache = result[0].value;
				return callback();
			}
		});
	}
}

loadPageFromDB(function() { console.log('Page generated successfully.'); }, 
			   function() { console.log('Page was not generated successfully.'); });

server.get(/[a-zA-Z0-9_\.\/~-]+/, function(req, res, next) {
	res.send(404, '404 - Snail trail not found.');
	return next();
});

server.get({ path: '/', version: '1.0.0'}, getFriendPage);
server.get({ path: '/friends', version: '1.0.0'}, getFriends);
server.post({ path: '/friends', version: '1.0.0'}, addFriend);
server.post({ path: '/scrape', version: '1.0.0'}, onScrape);

server.get('/lib/jquery.js', function(req, res, next) {
	writeOut(res, 200, 'application/x-javascript', jqueryJs);
	return next();
});

server.get('/lib/weld.js' , function(req, res, next) {
	writeOut(res, 200, 'application/x-javascript', weldJs);
	return next();
});

server.get('/style/style.css' , function(req, res, next) {
	writeOut(res, 200, 'text/css', css);
	return next();
});

server.get(/\/images\/([a-zA-z_]+\.[a-z]+)/, function (req, res, next) {
	var img = undefined; 
	try {
		img = fs.readFileSync(__dirname + '/images/' + req.params[0]);
	} catch (err) {
		res.send(404, '404 - Snail trail not found.');
		return next();
	}

	writeOut(res, 200, 'image/png', img);
	return next();
})

var port = process.env.PORT || 8080;
server.listen(port, function() {
    console.log('%s listening at %s on %s', server.name, server.url, port);
});
